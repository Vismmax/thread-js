import nodemailer from 'nodemailer';
import url from 'url';
import { host, port, user, pass } from '../../config/emailConfig';

const secure = port === 465;
const transporter = nodemailer.createTransport({
  host,
  port,
  secure,
  auth: {
    user,
    pass
  }
});

export const sendEmailLikePost = async (to, username, link) => {
  const text = `
  Hello.
  Your post was liked by ${username}.
  Post Link: ${link}
  `;

  const html = `
  <b>Hello.</b>
  <p>Your post was liked by ${username}.</p>
  <p>Post Link: <a href="${link}">post</a></p>
  `;

  const info = await transporter.sendMail({
    from: 'info@thread.com',
    to,
    subject: 'Like post',
    text,
    html
  });

  return info;
};

export const sendEmailLinkPost = async (to, username, link) => {
  const text = `
  Hello.
  ${username} shared a post with you.
  Have a look: ${link}
  `;

  const html = `
  <b>Hello.</b>
  <p>${username} shared a <a href="${link}">post</a> with you</p>
  <p>Have a look: <a href="${link}">post</a></p>
  `;

  const info = await transporter.sendMail({
    from: 'info@thread.com',
    to,
    subject: 'Like post',
    text,
    html
  });

  return info;
};

export const sendEmailResetPassword = async ({ id, email, username, password }, req) => {
  const id64 = Buffer.from(id).toString('base64');
  const pass64 = Buffer.from(password).toString('base64');
  const email64 = Buffer.from(email).toString('base64');
  const username64 = Buffer.from(username).toString('base64');
  const ref = new URL(req.get('referer'));
  const link = url.format({
    // protocol: req.protocol,
    protocol: ref.protocol,
    // host: req.get('host'),
    host: ref.host,
    pathname: '/password',
    query: {
      pass: pass64,
      id: id64,
      email: email64,
      username: username64
    }
  });

  const html = `
  <b>Hello.</b>
  <p>Password reset recovery link: <a href="${link}">reset</a></p>
  `;

  const info = await transporter.sendMail({
    from: 'info@thread.com',
    to: email,
    subject: 'Reset password',
    html
  });

  return info;
};
