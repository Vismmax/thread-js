import env from '../env';

export const { host, port, user, password: pass } = env.email;
