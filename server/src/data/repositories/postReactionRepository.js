import { PostReactionModel, PostModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getPostReactions(filter) {
    return this.model.findAll({
      group: [
        'postReaction.id',
        'post.id',
        'user.id',
        'user->image.id'
      ],
      where: filter,
      include: [{
        model: PostModel,
        attributes: ['id']
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
