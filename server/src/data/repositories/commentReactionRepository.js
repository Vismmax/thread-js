import { CommentReactionModel, CommentModel, UserModel, ImageModel, PostModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(filter) {
    return this.model.findAll({
      group: [
        'commentReaction.id',
        'comment.id',
        'comment->post.id',
        'user.id',
        'user->image.id'
      ],
      where: filter,
      include: [{
        model: CommentModel,
        attributes: ['id'],
        include: {
          model: PostModel,
          attributes: ['id']
        }
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
