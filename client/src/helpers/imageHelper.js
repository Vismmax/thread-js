export const getUserImgLink = image => (image
  ? image.link
  : 'https://i.imgur.com/l6zGE7c.png');
