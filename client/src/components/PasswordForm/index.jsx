import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

const PasswordForm = ({ params, update }) => {
  const [pass1, setPass1] = useState('');
  const [pass2, setPass2] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPassValid, setIsPassValid] = useState(true);

  const handleLoginClick = async () => {
    if (pass1 !== pass2 && pass1 !== '') {
      setIsPassValid(false);
      return;
    }
    if (isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await update({
        id: params.id,
        email: params.email,
        username: params.username,
        password: pass1,
        pass: params.pass });
    } catch {
      NotificationManager.error('Failed updated');
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          placeholder="New Password"
          type="password"
          value={pass1}
          error={!isPassValid}
          onChange={ev => setPass1(ev.target.value)}
        />
        <Form.Input
          fluid
          placeholder="Repeat password"
          type="password"
          value={pass2}
          error={!isPassValid}
          onChange={ev => setPass2(ev.target.value)}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Update
        </Button>
      </Segment>
    </Form>
  );
};

PasswordForm.propTypes = {
  params: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func.isRequired
};

export default PasswordForm;
