import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Container } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
import PopupUsers from '../PopupUsers';

const Comment = ({
  comment: { id, body, postId, createdAt, user, likeCount, dislikeCount, reactions },
  userId,
  likeComment,
  dislikeComment,
  updateComment,
  deleteComment,
  updateReactions
}) => {
  const getReactions = () => {
    const existReactions = likeCount + dislikeCount > 0;
    const existNewReactions = reactions?.length !== likeCount + dislikeCount;
    if (existReactions && (!reactions || existNewReactions)) updateReactions(id);
  };

  const ownCommentBtn = (
    <>
      <CommentUI.Action onClick={() => updateComment({ id, body })}>
        <Icon name="edit" />
      </CommentUI.Action>
      <CommentUI.Action onClick={() => deleteComment({ id, postId, userId })}>
        <Icon name="trash" />
      </CommentUI.Action>
    </>
  );

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <Container>
          <CommentUI.Author as="a">
            {user.username}
          </CommentUI.Author>
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
          </CommentUI.Metadata>
        </Container>
        <CommentUI.Metadata>
          {user.status}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <PopupUsers
            isLike
            reactions={reactions}
            onOpen={getReactions}
            likeCount={likeCount}
            trigger={(
              <CommentUI.Action onClick={() => likeComment(id)}>
                <Icon name="thumbs up" />
                {likeCount}
              </CommentUI.Action>
            )}
          />
          <PopupUsers
            isLike={false}
            reactions={reactions}
            onOpen={getReactions}
            dislikeCount={dislikeCount}
            trigger={(
              <CommentUI.Action onClick={() => dislikeComment(id)}>
                <Icon name="thumbs down" />
                {dislikeCount}
              </CommentUI.Action>
            )}
          />
          {user.id === userId && ownCommentBtn}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updateReactions: PropTypes.func.isRequired
};

export default Comment;
