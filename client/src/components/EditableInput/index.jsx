import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Segment, Input, Button, Icon, Popup } from 'semantic-ui-react';
import styles from './styles.module.scss';

const EditableInput = ({
  icon,
  placeholder,
  editable,
  value: val,
  validateValue,
  saveValue
}) => {
  const [value, setValue] = useState(val);
  const [isEdit, setIsEdit] = useState(false);
  const [isValidate, setIsValidate] = useState(false);
  const [isValidValue, setIsValidValue] = useState(true);
  const [messageError, setMessageError] = useState('');

  const handleChangeValue = async event => {
    setValue(event.target.value);
    setIsValidate(true);
    const res = await validateValue(event.target.value);
    setIsValidValue(res.valid);
    setMessageError(res.error);
    setIsValidate(false);
  };

  const handleClickBtn = async () => {
    setIsEdit(!isEdit);
    if (isEdit) {
      setIsValidate(true);
      await saveValue(value);
      setIsValidate(false);
    }
  };
  const handleClickBtnCancel = () => {
    setIsEdit(false);
    setValue(val);
    setIsValidate(false);
    setIsValidValue(true);
  };

  return (
    <Segment basic>
      <Popup
        content={messageError}
        position="right center"
        pinned
        className={styles.error}
        open={!isValidValue}
        trigger={(
          <Input
            icon
            iconPosition="left"
            placeholder={placeholder}
            type="text"
            disabled={!isEdit}
            value={value}
            action
            error={!isValidValue}
            onChange={handleChangeValue}
          >
            <input />
            <Icon name={icon} />
            <Button
              icon={isEdit ? 'save' : 'edit'}
              // basic
              // color={!isValidValue && 'red'}
              loading={isValidate}
              disabled={!isValidValue || !editable}
              onClick={handleClickBtn}
            />
            <Button
              className={!isEdit ? styles.hide : ''}
              icon="cancel"
              onClick={handleClickBtnCancel}
            />
          </Input>
        )}
      />
    </Segment>
  );
};

EditableInput.defaultProps = {
  editable: true
};

EditableInput.propTypes = {
  icon: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  editable: PropTypes.bool,
  value: PropTypes.string.isRequired,
  validateValue: PropTypes.func.isRequired,
  saveValue: PropTypes.func.isRequired
};

export default EditableInput;
