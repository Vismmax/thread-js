import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user,
  applyPost,
  applyUpdatedPost,
  applyDeletedPost,
  applyAddComment,
  applyUpdateComment,
  applyDeleteComment
}) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);
    socket.on('like_post', () => {
      NotificationManager.info('Your post was liked!');
    });
    socket.on('dislike_post', () => {
      NotificationManager.info('Your post was disliked!');
    });
    socket.on('update_likes_post', reaction => {
      if (reaction.userId !== id) {
        applyUpdatedPost(reaction.postId);
      }
    });
    socket.on('new_post', post => {
      if (post.userId !== id) {
        applyPost(post.id);
      }
    });
    socket.on('update_post', post => {
      if (post.userId !== id) {
        applyUpdatedPost(post.id);
      }
    });
    socket.on('delete_post', post => {
      if (post.userId !== id) {
        applyDeletedPost(post.id);
      }
    });
    socket.on('like_comment', reaction => {
      NotificationManager.info('Your comment was liked!');
      if (reaction.userId !== id) {
        applyUpdateComment(reaction.commentId);
      }
    });
    socket.on('dislike_comment', reaction => {
      NotificationManager.info('Your comment was disliked!');
      if (reaction.userId !== id) {
        applyUpdateComment(reaction.commentId);
      }
    });
    socket.on('update_likes_comment', reaction => {
      if (reaction.userId !== id) {
        applyUpdateComment(reaction.commentId);
      }
    });
    socket.on('new_comment', comment => {
      if (comment.userId !== id) {
        applyAddComment(comment.id);
      }
    });
    socket.on('update_comment', comment => {
      if (comment.userId !== id) {
        applyUpdateComment(comment.id);
      }
    });
    socket.on('delete_comment', comment => {
      if (comment.userId !== id) {
        applyDeleteComment(comment);
      }
    });
    socket.on('send_link', sent => {
      if (sent) NotificationManager.info('Message sent successfully');
      else NotificationManager.error('Message not sent');
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  applyUpdatedPost: PropTypes.func.isRequired,
  applyDeletedPost: PropTypes.func.isRequired,
  applyAddComment: PropTypes.func.isRequired,
  applyUpdateComment: PropTypes.func.isRequired,
  applyDeleteComment: PropTypes.func.isRequired
};

export default Notifications;
