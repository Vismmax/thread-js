import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdatedComment = ({ commentId, commentBody, close, updateComment }) => {
  const [body, setBody] = useState(commentBody);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    await updateComment({ commentId, body });
    setBody('');
    close();
  };

  return (
    <Modal size="tiny" open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Comment</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdateComment}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          <Button floated="right" color="blue" type="submit">
            Update
          </Button>
          <Button color="grey" onClick={close}>
            Cancel
          </Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatedComment.propTypes = {
  commentId: PropTypes.string.isRequired,
  commentBody: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

export default UpdatedComment;
