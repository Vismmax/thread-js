import React from 'react';
import PropTypes from 'prop-types';
import { Popup, List } from 'semantic-ui-react';
import Spinner from '../Spinner';
import styles from './styles.module.scss';

const PopupUsers = ({ trigger, onOpen, reactions, isLike, likeCount, dislikeCount }) => {
  const disabled = ((isLike && +likeCount === 0) || (!isLike && +dislikeCount === 0));
  const filtered = reactions.filter(reaction => reaction.isLike === isLike);

  const names = filtered.map(reaction => (
    <List.Item key={reaction.id}>
      {reaction.user.username}
    </List.Item>
  ));

  return (
    <Popup className={styles.popup} trigger={trigger} onOpen={onOpen} disabled={disabled}>
      {names.length > 0
        ? (
          <>
            <Popup.Header>
              Users who
              {isLike ? ' liked' : ' disliked'}
            </Popup.Header>
            <Popup.Content>
              <List>
                {names}
              </List>
            </Popup.Content>
          </>
        )
        : <Spinner />}
    </Popup>
  );
};

PopupUsers.defaultProps = {
  reactions: [],
  likeCount: undefined,
  dislikeCount: undefined
};

PopupUsers.propTypes = {
  trigger: PropTypes.objectOf(PropTypes.any).isRequired,
  onOpen: PropTypes.func.isRequired,
  isLike: PropTypes.bool.isRequired,
  reactions: PropTypes.arrayOf(PropTypes.any),
  likeCount: PropTypes.string,
  dislikeCount: PropTypes.string
};

export default PopupUsers;
