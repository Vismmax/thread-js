import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'semantic-ui-react';

import styles from './styles.module.scss';

const PanelFilters = ({ updateFilter }) => {
  const [filter, setFilter] = useState({
    showOwnPosts: false,
    hideOwnPosts: false,
    showLikedPosts: false
  });
  useEffect(() => updateFilter(filter), [filter]);

  const toggleShowOwnPosts = () => {
    setFilter({
      ...filter,
      showOwnPosts: !filter.showOwnPosts,
      hideOwnPosts: filter.showOwnPosts ? filter.hideOwnPosts : filter.showOwnPosts
    });
  };

  const toggleHideOwnPosts = () => {
    setFilter({
      ...filter,
      hideOwnPosts: !filter.hideOwnPosts,
      showOwnPosts: filter.hideOwnPosts ? filter.showOwnPosts : filter.hideOwnPosts
    });
  };

  const toggleShowLikedPosts = () => {
    setFilter({
      ...filter,
      showLikedPosts: !filter.showLikedPosts
    });
  };

  return (
    <div className={styles.toolbar}>
      <Checkbox
        toggle
        label="Show only my posts"
        checked={filter.showOwnPosts}
        onChange={toggleShowOwnPosts}
      />
      <Checkbox
        toggle
        label="Hide my posts"
        checked={filter.hideOwnPosts}
        onChange={toggleHideOwnPosts}
      />
      <Checkbox
        toggle
        label="Liked by me"
        checked={filter.showLikedPosts}
        onChange={toggleShowLikedPosts}
      />
    </div>
  );
};

PanelFilters.propTypes = {
  updateFilter: PropTypes.func.isRequired
};

export default PanelFilters;
