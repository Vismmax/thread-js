import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Icon, Button } from 'semantic-ui-react';
import styles from './styles.module.scss';

const Status = ({ status, updateStatus }) => {
  const [value, setValue] = useState(status);
  const [isEdit, setIsEdit] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClickEdit = () => {
    setIsEdit(true);
  };

  const handleClickSave = async () => {
    setLoading(true);
    await updateStatus({ status: value });
    setLoading(false);
    setIsEdit(false);
  };

  const handleClickCancel = () => {
    setIsEdit(false);
  };

  const handleChangeValue = async event => {
    setValue(event.target.value);
  };

  return (
    <>
      <div
        className={`${styles.status} ${isEdit ? styles.hide : ''}`}
        onClick={e => e.preventDefault()}
      >
        {status}
        <Icon
          className={styles.icon}
          name="pencil alternate"
          onClick={handleClickEdit}
        />
      </div>
      <Input
        className={!isEdit ? styles.hide : ''}
        type="text"
        placeholder="Status"
        value={value}
        action
        onChange={handleChangeValue}
      >
        <input />
        <Button
          icon="save"
          loading={loading}
          onClick={handleClickSave}
        />
        <Button
          icon="cancel"
          onClick={handleClickCancel}
        />
      </Input>
    </>
  );
};

Status.propTypes = {
  status: PropTypes.string.isRequired,
  updateStatus: PropTypes.func.isRequired
};

export default Status;
