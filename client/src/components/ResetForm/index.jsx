import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

const ResetForm = ({ reset }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleLoginClick = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await reset({ email });
    } catch {
      NotificationManager.error('Failed reset');
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Reset
        </Button>
      </Segment>
    </Form>
  );
};

ResetForm.propTypes = {
  reset: PropTypes.func.isRequired
};

export default ResetForm;
