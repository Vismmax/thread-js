import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import PopupUsers from '../PopupUsers';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  userId,
  editPost,
  deletePost,
  expandedPost,
  updateReactions
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    reactions
  } = post;
  const date = moment(createdAt).fromNow();
  const getReactions = () => {
    // const existReactions = likeCount > 0 || dislikeCount > 0;
    const existReactions = likeCount + dislikeCount > 0;
    const existNewReactions = reactions?.length !== likeCount + dislikeCount;
    // if ((existReactions && !reactions) || (existReactions && existNewReactions)) updateReactions(id);
    if (existReactions && (!reactions || existNewReactions)) updateReactions(id);
  };

  const ownPostBtn = (
    <div className="right floated">
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editPost(post)}>
        <Icon name="edit" />
      </Label>
      <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
        <Icon name="trash" />
      </Label>
    </div>
  );
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <PopupUsers
          isLike
          reactions={reactions}
          onOpen={getReactions}
          likeCount={likeCount}
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        />
        <PopupUsers
          isLike={false}
          reactions={reactions}
          onOpen={getReactions}
          dislikeCount={dislikeCount}
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          )}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {post.user.id === userId && !expandedPost && ownPostBtn}
      </Card.Content>
    </Card>
  );
};

Post.defaultProps = {
  expandedPost: false
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  updateReactions: PropTypes.func.isRequired,
  expandedPost: PropTypes.bool
};

export default Post;
