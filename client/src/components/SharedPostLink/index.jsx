import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon, Segment, Header } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import { sharePostLink } from '../../services/postService';
import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  const [isValidEmail, setIsValidEmail] = useState(true);
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  const validateEmail = mail => {
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(mail);
  };

  const sendToEmail = () => {
    const link = `${window.location.origin}/share/${postId}`;
    const valid = validateEmail(email);
    setIsValidEmail(valid);
    if (valid) {
      sharePostLink({
        email,
        link
      });
    } else {
      NotificationManager.error('Email not valid');
    }
  };

  const handleChangeEmail = event => {
    setEmail(event.target.value);
    setIsValidEmail(true);
  };

  const handleFocusEmail = () => {
    setIsValidEmail(true);
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Segment basic>
          <Header size="medium">Copy link to clipboard</Header>
          <Input
            fluid
            action={{
              color: 'teal',
              labelPosition: 'right',
              icon: 'copy',
              content: 'Copy',
              onClick: copyToClipboard
            }}
            value={`${window.location.origin}/share/${postId}`}
            ref={ref => { input = ref; }}
          />
        </Segment>
        <Segment basic>
          <Header size="medium">Send link to email</Header>
          <Input
            fluid
            placeholder="Enter email"
            error={!isValidEmail}
            action={{
              color: 'teal',
              labelPosition: 'right',
              icon: 'mail',
              content: 'Send',
              disabled: !isValidEmail,
              onClick: sendToEmail
            }}
            onFocus={handleFocusEmail}
            onChange={handleChangeEmail}
            value={email}
          />
        </Segment>

      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
