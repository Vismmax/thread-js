import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Form, Image, Button } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

import styles from './styles.module.scss';

const UpdatedPost = ({ postID, postBody, close, updatePost, uploadImage }) => {
  const [body, setBody] = useState(postBody);
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost({ postID, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      NotificationManager.error('Failed updated');
      setIsUploading(false);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdatePost}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.imageLink} alt="post" />
            </div>
          )}
          <Button
            color="teal"
            icon
            labelPosition="left"
            as="label"
            loading={isUploading}
            disabled={isUploading}
          >
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handleUploadFile} hidden />
          </Button>
          <Button floated="right" color="blue" type="submit">
            Update
          </Button>
          <Button floated="right" color="grey" onClick={close}>
            Cancel
          </Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatedPost.propTypes = {
  postID: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatedPost;
