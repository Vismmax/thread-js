import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { updatePassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import PasswordForm from '../../components/PasswordForm';

const DecodeParams = search => {
  const params = {};
  const str = search.slice(1);
  const arr = str.split('&');
  const arrParams = arr.map(item => {
    const idx = item.indexOf('=');
    const key = item.slice(0, idx);
    const value = item.slice(idx + 1).replace(/%3D/g, '=');
    return { key, value };
  });
  arrParams.forEach(item => {
    const value = Buffer.from(item.value, 'base64').toString('ascii');
    params[item.key] = value;
  });
  return params;
};

const PasswordPage = ({ updatePassword: updatePass }) => {
  const history = useHistory();
  const location = useLocation();
  const params = DecodeParams(location.search);

  const update = async data => {
    await updatePass(data);
    history.push('/login');
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          New Password
        </Header>
        <PasswordForm params={params} update={update} />
      </Grid.Column>
    </Grid>
  );
};

PasswordPage.propTypes = {
  updatePassword: PropTypes.func.isRequired
};

const actions = { updatePassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(PasswordPage);
