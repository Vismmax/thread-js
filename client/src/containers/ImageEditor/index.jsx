import React, { useState, useEffect, useCallback, useRef } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/lib/ReactCrop.scss';
import PropTypes from 'prop-types';
import { Modal, Button, Icon, Accordion } from 'semantic-ui-react';
import styles from './styles.module.scss';

const pixelRatio = 4;

const getResizedCanvas = (canvas, newWidth, newHeight) => {
  const tmpCanvas = document.createElement('canvas');
  tmpCanvas.width = newWidth;
  tmpCanvas.height = newHeight;

  const ctx = tmpCanvas.getContext('2d');
  ctx.drawImage(
    canvas,
    0,
    0,
    canvas.width,
    canvas.height,
    0,
    0,
    newWidth,
    newHeight
  );

  return tmpCanvas;
};

const ImageEditor = ({ src, updateImage, open, handleError, closeImageEditor }) => {
  const [upImg, setUpImg] = useState(src);
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: 'px',
    width: 100,
    height: 100,
    maxWidth: 200,
    maxHeight: 200,
    aspect: 1 });
  const [completedCrop, setCompletedCrop] = useState(null);
  const [isUploading, setIsUploading] = useState(false);
  const [isShowResult, setIsShowResult] = useState(false);
  const [isSaving, setIsSaving] = useState(false);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      setIsUploading(true);
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    const crossImg = img;
    crossImg.crossOrigin = 'Anonymous';
    imgRef.current = crossImg;
    setIsUploading(false);
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const cropIm = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');

    canvas.width = cropIm.width * pixelRatio;
    canvas.height = cropIm.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingEnabled = false;

    ctx.drawImage(
      image,
      cropIm.x * scaleX,
      cropIm.y * scaleY,
      cropIm.width * scaleX,
      cropIm.height * scaleY,
      0,
      0,
      cropIm.width,
      cropIm.height
    );
  }, [completedCrop]);

  const saveImage = () => {
    if (!completedCrop || !previewCanvasRef.current) {
      handleError('Something went wrong');
      return;
    }

    setIsSaving(true);

    const canvas = getResizedCanvas(previewCanvasRef.current, completedCrop.width, completedCrop.height);

    try {
      canvas.toBlob(
        blob => {
          updateImage(blob)
            .then(() => closeImageEditor())
            .catch(() => handleError('Image not updated'))
            .finally(() => setIsSaving(false));
        },
        'image/png',
        1
      );
    } catch {
      handleError('Download a new image');
    }
  };

  return (
    <Modal size="small" dimmer centered={false} open={open} onClose={closeImageEditor}>
      <Modal.Header>Edit user photo</Modal.Header>
      <Modal.Content>
        <div className={styles.wrap}>
          <ReactCrop
            ruleOfThirds
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={setCrop}
            onComplete={setCompletedCrop}
            maxWidth="200"
            maxHeight="200"
          />
        </div>
        <Accordion className={styles.wrap}>
          <Accordion.Title active={isShowResult} onClick={() => setIsShowResult(!isShowResult)}>
            <Button primary compact size="mini">
              {isShowResult ? 'Hide ' : 'Show '}
              result
            </Button>
          </Accordion.Title>
          <Accordion.Content active={isShowResult}>
            <canvas
              className={styles.result}
              ref={previewCanvasRef}
              style={{
                width: completedCrop?.width ?? 0,
                height: completedCrop?.height ?? 0
              }}
            />
          </Accordion.Content>
        </Accordion>

        <Button
          color="teal"
          icon
          labelPosition="left"
          as="label"
          loading={isUploading}
          disabled={isUploading || isSaving}
        >
          <Icon name="image" />
          Load image
          <input name="image" type="file" onChange={onSelectFile} hidden />
        </Button>
        <Button
          floated="right"
          color="blue"
          type="submit"
          disabled={!completedCrop?.width || !completedCrop?.height || isSaving}
          loading={isSaving}
          onClick={() => saveImage(previewCanvasRef.current, completedCrop)}
        >
          Save
        </Button>
        <Button
          floated="right"
          color="grey"
          type="submit"
          disabled={isSaving}
          onClick={closeImageEditor}
        >
          Cancel
        </Button>
      </Modal.Content>
    </Modal>
  );
};

ImageEditor.propTypes = {
  open: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
  updateImage: PropTypes.func.isRequired,
  handleError: PropTypes.func.isRequired,
  closeImageEditor: PropTypes.func.isRequired
};

export default ImageEditor;
