import React from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { resetPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import ResetForm from '../../components/ResetForm';

const ResetPage = ({ resetPassword: resetPass }) => {
  const history = useHistory();

  const reset = async data => {
    await resetPass(data);
    history.push('/login');
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Reset Password
        </Header>
        <ResetForm reset={reset} />
      </Grid.Column>
    </Grid>
  );
};

ResetPage.propTypes = {
  resetPassword: PropTypes.func.isRequired
};

const actions = { resetPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ResetPage);
