import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  updateComment,
  deleteComment,
  updateCommentReactions,
  updatePostReactions
} from 'src/containers/Thread/actions';
import UpdatedComment from '../../components/UpdateComment';

const ExpandedPost = ({
  post,
  userId,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  likeComment: likeCommt,
  dislikeComment: dislikeCommt,
  updateComment: updateCommt,
  deleteComment: deleteCommt,
  updatePostReactions: updateReactions,
  updateCommentReactions: updateCommtReactions
}) => {
  const [editableComment, setEditableComment] = useState(undefined);

  const editComment = comment => {
    setEditableComment(comment);
  };

  return (
    <Modal dimmer centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              userId={userId}
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              editPost={() => {}}
              deletePost={() => {}}
              updateReactions={updateReactions}
              expandedPost
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    userId={userId}
                    likeComment={likeCommt}
                    dislikeComment={dislikeCommt}
                    updateComment={editComment}
                    deleteComment={deleteCommt}
                    updateReactions={updateCommtReactions}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
            {editableComment && (
              <UpdatedComment
                commentId={editableComment.id}
                commentBody={editableComment.body}
                updateComment={updateCommt}
                close={() => setEditableComment(undefined)}
              />
            )}
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  updatePostReactions: PropTypes.func.isRequired,
  updateCommentReactions: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  updateComment,
  deleteComment,
  updateCommentReactions,
  updatePostReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
