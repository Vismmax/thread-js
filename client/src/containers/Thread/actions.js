import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import { ADD_POST, DELETE_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST, UPDATE_POST } from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const applyUpdatedPost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);
  dispatch(updatePostAction(updatedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const updatePost = post => async dispatch => {
  const { id } = await postService.updatePost(post);
  dispatch(applyUpdatedPost(id));
};

export const applyDeletedPost = postId => async (dispatch, getRootState) => {
  dispatch(deletePostAction(postId));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    const deletedPost = {
      ...expandedPost,
      body: 'Post deleted'
    };
    dispatch(setExpandedPostAction(deletedPost));
  }
};

export const deletePost = postId => async dispatch => {
  const deletedPost = await postService.deletePost({ id: postId });
  dispatch(deletePostAction(deletedPost.id));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDis = (diff === 1 && createdAt !== updatedAt) ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + diffDis
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffLike = (diff === 1 && createdAt !== updatedAt) ? -1 : 0;

  const mapDislike = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLike,
    dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislike(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislike(expandedPost)));
  }
};

export const applyAddComment = id => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

// export const addComment = request => async (dispatch, getRootState) => {
//   const { id } = await commentService.addComment(request);
//   const comment = await commentService.getComment(id);
//
//   const mapComments = post => ({
//     ...post,
//     commentCount: Number(post.commentCount) + 1,
//     comments: [...(post.comments || []), comment] // comment is taken from the current closure
//   });
//
//   const { posts: { posts, expandedPost } } = getRootState();
//   const updated = posts.map(post => (post.id !== comment.postId
//     ? post
//     : mapComments(post)));
//
//   dispatch(setPostsAction(updated));
//
//   if (expandedPost && expandedPost.id === comment.postId) {
//     dispatch(setExpandedPostAction(mapComments(expandedPost)));
//   }
// };

export const addComment = request => async dispatch => {
  const { id } = await commentService.addComment(request);
  dispatch(applyAddComment(id));
};

export const applyUpdateComment = id => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: post.comments.map(cmt => (cmt.id === comment.id ? comment : cmt))
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = request => async dispatch => {
  const { id } = await commentService.updateComment(request);
  dispatch(applyUpdateComment(id));
};

export const applyDeleteComment = comment => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  const mapComments = post => ({
    ...post,
    commentCount: post.commentCount - 1,
    comments: post.comments.filter(cmt => cmt.id !== comment.id)
  });

  const updatedPosts = posts.map(post => (post.id !== comment.postId
    ? post
    : {
      ...post,
      commentCount: post.commentCount - 1
    }));

  dispatch(setPostsAction(updatedPosts));
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = delComment => async dispatch => {
  const comment = await commentService.deleteComment(delComment);
  dispatch(applyDeleteComment(comment));
};

const updateLakedComment = (diffLike, diffDis, commentId, getRootState) => {
  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLike,
    dislikeCount: Number(comment.dislikeCount) + diffDis
  });

  const mapPosts = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
  });

  const { posts: { expandedPost } } = getRootState();
  return mapPosts(expandedPost);
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(commentId);
  const diffLike = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  const diffDis = (diffLike === 1 && createdAt !== updatedAt) ? -1 : 0;

  const updatedPost = updateLakedComment(diffLike, diffDis, commentId, getRootState);
  dispatch(setExpandedPostAction(updatedPost));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.dislikeComment(commentId);
  const diffDis = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  const diffLike = (diffDis === 1 && createdAt !== updatedAt) ? -1 : 0;

  const updatedPost = updateLakedComment(diffLike, diffDis, commentId, getRootState);
  dispatch(setExpandedPostAction(updatedPost));
};

export const updatePostReactions = postId => async (dispatch, getRootState) => {
  const reactions = await postService.getReactions(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const post = posts.find(item => item.id === postId);
  const updatedPost = {
    ...post,
    reactions
  };
  dispatch(updatePostAction(updatedPost));

  if (expandedPost && expandedPost.id === postId) {
    const updatedExpandedPost = {
      ...expandedPost,
      reactions
    };
    dispatch(setExpandedPostAction(updatedExpandedPost));
  }
};

export const updateCommentReactions = commentId => async (dispatch, getRootState) => {
  const reactions = await commentService.getReactions(commentId);

  const { posts: { expandedPost } } = getRootState();
  const comments = expandedPost.comments.map(comment => (
    comment.id !== commentId
      ? comment
      : {
        ...comment,
        reactions
      }));
  const updatedExpandedPost = {
    ...expandedPost,
    comments
  };
  dispatch(setExpandedPostAction(updatedExpandedPost));
};
