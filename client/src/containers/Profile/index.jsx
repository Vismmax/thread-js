import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid, Image } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import { getUserImgLink } from 'src/helpers/imageHelper';
import * as imageService from '../../services/imageService';
import { getUserByName } from '../../services/authService';
import { updateCurrentUser } from './actions';
import ImageEditor from '../ImageEditor';
import EditableInput from '../../components/EditableInput';

const Profile = ({
  user,
  updateCurrentUser: updateUser
}) => {
  const [isOpenImageEditor, setIsOpenImageEditor] = useState(false);

  const updateImage = async file => {
    const { id: imageId } = await imageService.uploadImage(file);
    updateUser({
      ...user,
      imageId
    });
  };

  const handleError = error => NotificationManager.error(error);

  const validateUsername = async username => {
    const validateInvalidName = /^[\w-]{3,}$/.test(username);
    if (!validateInvalidName) {
      return {
        valid: false,
        error: 'Invalid name'
      };
    }

    const validatedUser = await getUserByName({ username });
    if (validatedUser.username) {
      return {
        valid: false,
        error: 'Name exists'
      };
    }

    return {
      valid: true,
      error: ''
    };
  };

  const validateEmail = async () => ({
    valid: true,
    error: ''
  });

  const validateStatus = async () => ({
    valid: true,
    error: ''
  });

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          circular
          size="medium"
          src={getUserImgLink(user.image)}
          onClick={() => setIsOpenImageEditor(true)}
        />
        <EditableInput
          icon="user"
          placeholder="Username"
          value={user.username}
          validateValue={validateUsername}
          saveValue={val => updateUser({ username: val })}
        />
        <EditableInput
          icon="at"
          placeholder="Email"
          editable={false}
          value={user.email}
          validateValue={validateEmail}
          saveValue={val => updateUser({ email: val })}
        />
        <EditableInput
          icon="address card"
          placeholder="Status"
          value={user.status}
          validateValue={validateStatus}
          saveValue={val => updateUser({ status: val })}
        />
        <ImageEditor
          open={isOpenImageEditor}
          src={getUserImgLink(user.image)}
          updateImage={updateImage}
          handleError={handleError}
          closeImageEditor={() => { setIsOpenImageEditor(false); }}
        />
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateCurrentUser: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateCurrentUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
